import boto3
import sys
import re
import json
import datetime


REGION = sys.argv[1]
client = boto3.client("accessanalyzer", region_name=REGION)

def main():
    analyzers = client.list_analyzers(
        type="ORGANIZATION"
    )["analyzers"]
    org_analyzer_arn = [analyzer["arn"] for analyzer in analyzers][0]
    findings = get_findings(org_analyzer_arn)
    active_findings = get_active_findings(findings)
    # print(json.dumps(active_findings, default=datetime_tostring))
    print(f"Total active findings: {len(active_findings)}")
    principal_findings = get_principal_findings(active_findings)
    print(f"Total principal findings: {len(principal_findings)}")
    federated_findings = get_federated_findings(principal_findings)
    print(f"Total federated findings: {len(federated_findings)}")
    filtered_findings = get_federation_findings(federated_findings)
    print(f"Total filtered findings: {len(filtered_findings)}")
    # ids = get_ids(filtered_findings)
    # print(ids)
    archive_findings(filtered_findings, org_analyzer_arn)

def get_findings(analyzer_arn):
    findings = []
    paginator = client.get_paginator("list_findings")
    page_iterator = paginator.paginate(
        analyzerArn=analyzer_arn
    )
    for page in page_iterator:
        findings += page["findings"]
    return findings
    
def get_active_findings(findings):
    return [
        finding for finding in findings
        if finding["status"] == "ACTIVE"
    ]

def get_principal_findings(findings):
    return [
        finding for finding in findings
        if "principal" in finding.keys()
    ]

def get_federated_findings(findings):
    return [
        finding for finding in findings
        if "Federated" in finding["principal"].keys()
    ]

def get_federation_findings(findings):
    return [
        finding for finding in findings
        if is_valid_federation(finding["principal"]["Federated"])
    ]

def archive_findings(findings, analyzer):
    if findings:
        finding_ids = get_ids(findings)
        client.update_findings(
            analyzerArn=analyzer,
            ids=finding_ids,
            status="ARCHIVED"
        )

def get_ids(findings):
    return [finding["id"] for finding in findings]

def is_valid_federation(text):
    pattern = "saml-provider/"
    return True if re.search(pattern, text) else False

def datetime_tostring(obj):
    if isinstance(obj, datetime.datetime):
        return obj.__str__()

if __name__ == "__main__":
    main()
