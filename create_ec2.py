import boto3


def assume_role():
    sts = boto3.client("sts")
    response = sts.assume_role(
        RoleArn="arn:aws:iam::820696150685:role/Admin-JamesChung",
        RoleSessionName="AdminSession"
    )
    return response;


assumed_role = assume_role()

access_key_id = assumed_role["Credentials"]["AccessKeyId"]
secret_access_key = assumed_role["Credentials"]["SecretAccessKey"]
session_token = assumed_role["Credentials"]["SessionToken"]

ec2 = boto3.resource(
    service_name="ec2",
    aws_access_key_id=access_key_id,
    aws_secret_access_key=secret_access_key,
    aws_session_token=session_token
)
iam = boto3.client(
    service_name="iam",
    aws_access_key_id=access_key_id,
    aws_secret_access_key=secret_access_key,
    aws_session_token=session_token
)
print(iam.list_instance_profiles())
iam_resource = boto3.resource(
    service_name="iam",
    aws_access_key_id=access_key_id,
    aws_secret_access_key=secret_access_key,
    aws_session_token=session_token
)


def main():
    role_name = "AmazonSSMRoleForInstances"
    tag_values = ["hydra", "avengers", "peons"]
    instance_count = 2
    for tag_value in tag_values:
        create_ec2_instances(role_name, instance_count, tag_value)


def create_ec2_instances(role_name, instance_count, tag_value):
    response = ec2.create_instances(
        ImageId="ami-062f7200baf2fa504",
        InstanceType="t2.micro",
        MaxCount=instance_count,
        MinCount=instance_count,
        TagSpecifications=create_instance_tag_specification(tag_value),
        IamInstanceProfile={
            "Arn": "",
            "Name": role_name
        }
    )
    return response


def create_instance_tag_specification(tag_value):
    tag_specification = [
        {
            "ResourceType": "instance",
            "Tags": [
                {
                    "Key": "Name",
                    "Value": f"team-{tag_value}"
                },
                {
                    "Key": "team",
                    "Value": tag_value
                }
            ]
        }
    ]
    return tag_specification


if __name__ == "__main__":
    main()
